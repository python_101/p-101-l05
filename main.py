# Chapter 8
# Conditional Logic and Control Flow
# 8.1 Compare Values
# 1. For each of the following conditional expressions, guess whether they evaluate to True or False.
# Then type them into the interactive window to check your answers:
# 1 <= 1
# True
# 1 != 1
# False
# 1 != 2
# True
# "good" != "bad"
# True
# "good" != "Good"
# True
# 123 == "123"
# False
# 2. For each of the following expressions, fill in the blank (indicated by __) with an appropriate boolean
# comparator so that the expression evaluates to True:
# 3 __ 4
# !=
# 10 __ 5
# !=
# "jack" __ "jill"
# !=
# 42 __ "42"
# !=

# 8.2 Add Some Logic
# 1. Figure out what the result will be (True or False) when evaluating the following expressions, then type them into
# the interactive window to check your answers:
# (1 <= 1) and (1 != 1)
# False
# not (1 != 2)
# False
# ("good" != "bad") or False
# True
# ("good" != "Good") and not (1 == 1)
# False
# 2. Add parentheses where necessary so that each of the following expressions evaluates to True:
# False == (not True)
# (True and False) == (True and False)
# not (True and "A" == "B")

# 8.3 Control the Flow of Your Program
# 1. Write a script that prompts the user to enter a word using the input() function, stores that input in a variable,
# and then displays whether the length of that string is less than 5 characters, greater than 5 characters, or equal to
# 5 characters by using a set of if, elif and else statements.
prompt = input("Write a word, and I will tell you if it is equal to 5 or greater/less than 5 characters: ")
prompt_len = len(prompt)
if prompt_len == 5:
    print(f"{prompt} is equal 5")
elif prompt_len > 5:
    print(f"{prompt} is greater than 5")
else:
    print(f"{prompt} is less than 5")

# 8.4 Challenge: Find the Factors of a Number
# A factor of a positive integer n is any positive integer less than or equal to n that divides n with no remainder.
# For example, 3 is a factor of 12 because 12 divided by 3 is 4, with no remainder. However, 5 is not a factor of 12
# because 5 goes into 12 twice with a remainder of 2.
# Write a script factors.py that asks the user to input a positive integer and then prints out the factors of that
# number. Here’s a sample run of the program with output:
# Enter a positive integer: 12
# 1 is a factor of 12
# 2 is a factor of 12
# 3 is a factor of 12
# 4 is a factor of 12
# 6 is a factor of 12
# 12 is a factor of 12
# Hint: Recall from Chapter 5 that you can use the % operator to get the remainder of dividing one number by another.
positive_int = int(input("Enter a positive integer: "))
for i in range(1, (positive_int + 1)):
    if positive_int % i == 0:
        print(f"{i} is a factor of {positive_int}")

# 8.5 Break Out of the Pattern
# 1. Using break, write a program that repeatedly asks the user for some input and only quits if the user enters
# "q" or "Q".
while True:
    password = input("You can write everything, but when you want to quit press q/Q and press enter: ")
    password = password.lower()
    if password == "q":
        break
# 2. Using continue, write a program that loops over the numbers 1 to 50 and prints all numbers that are
# not multiples of 3.
for i in range(1, 51):
    if i % 3 == 0:
        continue
    print(i)

# 8.6 Recover From Errors
# 1. Write a script that repeatedly asks the user to input an integer, displaying a message to “try again” by catching
# the ValueError that is raised if the user did not enter an integer. Once the user enters an integer, the program
# should display the number back to the user and end without crashing.
while True:
    try:
        prompt = int(input("Enter an integer: "))
        break
    except ValueError:
        print("Try Again")

# 2. Write a program that asks the user to input a string and an integer n. Then display the character at index n in the
# string. Use error handling to make sure the program doesn't crash if the user does not enter an integer or the index
# is out of bounds. The program should display a different message depending on what error occurs.
try:
    string1 = str(input("Input a string: "))
    integer1 = int(input("Input a integer: "))
    print(string1[integer1])
except ValueError:
    print("Please input string first and integer second")
except IndexError:
    print("You entered index out of bounds")

# 8.7 Simulate Events and Calculate Probabilities
# 1. Write a function called roll() that uses the randint() function to simulate rolling a fair die by returning a
# random integer between 1 and 6.
import random


def roll():
    return random.randint(1, 6)


print(roll())
# 2. Write a script that simulates 10,000 rolls of a fair die and displays the average number rolled.
import random


def dice_roll():
     """Randomly roll dice"""
    if random.randint(1, 6) == 1:
        return "one"
    elif random.randint(1, 6) == 2:
        return "two"
    elif random.randint(1, 6) == 3:
        return "three"
    elif random.randint(1, 6) == 4:
        return "four"
    elif random.randint(1, 6) == 5:
        return "five"
    else:
        return "six"


one_tally = 0
two_tally = 0
three_tally = 0
four_tally = 0
five_tally = 0
six_tally = 0

for trail in range(10_000):
    if dice_roll() == "one":
        one_tally = one_tally + 1
    elif dice_roll() == "two":
        two_tally = two_tally + 1
    elif dice_roll() == "three":
        three_tally = three_tally + 1
    elif dice_roll() == "four":
        four_tally = four_tally + 1
    elif dice_roll() == "five":
        five_tally = five_tally + 1
    else:
        six_tally = six_tally + 1


print(f"The average number of 1 = {one_tally}")
print(f"The average number of 2 = {two_tally}")
print(f"The average number of 3 = {three_tally}")
print(f"The average number of 4 = {four_tally}")
print(f"The average number of 5 = {five_tally}")
print(f"The average number of 6 = {six_tally}")

# 8.8 Challenge: Simulate a Coin Toss Experiment
# Suppose you flip a fair coin repeatedly until it lands on both heads and tails at least once each.
# In other words, after the first flip, you continue to flip the coin until it lands on something different.
# Doing this generates a sequence of heads and tails. For example, the first time you do this experiment, the sequence
# might be heads, heads,then tails.
# On average, how many flips are needed for the sequence to contain both heads and tails?
# Write a simulation that runs 10,000 trials of the experiment and prints the average number of flips per trial.
import random


def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


flips = 0
num_trials = 10_000


for trial in range(num_trials):
    if coin_flip() == "heads":
        flips += 1
        while coin_flip() == "heads":
            flips += 1
        flips += 1
    else:
        flips += 1
        while coin_flip() == "tails":
            flips += 1
        flips += 1
avg_flips_per_trial = flips / num_trials
print(f"The average number of flips per trial is {avg_flips_per_trial}")

# With some help from the random module and a little condition logic, you can simulate an election between two
# candidates. Suppose two candidates, Candidate A and Candidate B, are running for mayor in a city with three voting
# regions. The most recent polls show that Candidate A has the following chances for winning in each region:
# • Region 1: 87% chance of winning
# • Region 2: 65% chance of winning
# • Region 3: 17% chance of winning
# Write a program that simulates the election 10,000 times and prints the percentage of where Candidate A wins.
# To keep things simple, assume that a candidate wins the election is they win in at least two of the three regions.
# Simulate the results of an election using a Monte Carlo simulation

from random import random

num_times_A_wins = 0
num_times_B_wins = 0

num_trials = 10_000
for trial in range(0, num_trials):
    votes_for_A = 0
    votes_for_B = 0

    if random() < 0.87:
        votes_for_A += 1
    else:
        votes_for_B += 1

    if random() < 0.65:
        votes_for_A += 1
    else:
        votes_for_B += 1

    if random() < 0.17:
        votes_for_A += 1
    else:
        votes_for_B += 1

    if votes_for_A > votes_for_B:
        num_times_A_wins += 1
    else:
        num_times_B_wins += 1

print(f"Probability A wins: {num_times_A_wins / num_trials}")
print(f"Probability B wins: {num_times_B_wins / num_trials}")

# Chapter 9 Tuples, Lists, and Dictionaries
# 9.1 Tuples Are Immutable Sequences
# 1. Create a tuple literal named cardinal_numbers that holds the strings "first", "second" and "third", in that order.
cardinal_numbers = ("first", "second", "third")
# 2. Using index notation and print(), display the string at index 1 in cardinal_numbers.
print(cardinal_numbers[1])
# 3. Unpack the values in cardinal_numbers into three new strings named position1, position2 and position3 in a single
# line of code, then print each value on a separate line.
position1, position2, position3 = cardinal_numbers
print(position1)
print(position2)
print(position3)
# 4. Create a tuple called my_name that contains the letters of your name by using tuple() and a string literal.
my_name = ("Mateusz", )
# 5. Check whether or not the character "x" is in my_name using the in keyword.
print("x" in my_name)
# 6. Create a new tuple containing all but the first letter in my_name using slicing notation.
print(my_name[1:])

# 9.2 Lists Are Mutable Sequences
# 1. Create a list named food with two elements "rice" and "beans".
food = ["rice", "beans"]
# 2. Append the string "broccoli" to food using .append().
food.append("broccoli")
# 3. Add the string "bread" and "pizza" to "food" using .extend().
food.extend(["bread", "pizza"])
# 4. Print the first two items in the food list using print() and slicing notation.
print(food[:2])
# 5. Print the last item in food using print() and index notation.
print(food[-1])
# 6. Create a list called breakfast from the string "eggs, fruit, orange juice" using the string .split() method.
breakfast = "eggs", "fruit", "orange juice".split(", ")
# 7. Verify that breakfast has three items using len().
print(len(breakfast) == 3)
# 8. Create a new list called lengths using a list comprehension that contains the lengths of each string in the
# breakfast list.
lengths = [len(word) for word in breakfast]
print(lengths)

# 9.3 Nesting, Copying, and Sorting Tuples and Lists
# 1. Create a tuple data with two values. The first value should be the tuple (1, 2) and the second value should be the
# tuple (3, 4).
my_tuple = ((1, 2), (3, 4))
# 2. Write a for loop that loops over data and prints the sum of each nested tuple. The output should look like this:
# Row 1 sum: 3
# Row 2 sum: 7
first = sum(my_tuple[0])
second = sum(my_tuple[1])
print(f"Row 1 sum: {first}")
print(f"Row 2 sum: {second}")
# 3. Create the following list [4, 3, 2, 1] and assign it to the variable numbers.
numbers = [4, 3, 2, 1]
# 4. Create a copy of the numbers list using the [:] slicing notation.
numbers_two = numbers[:]
# 5. Sort the numbers list in numerical order using the .sort() method.
numbers.sort()
print(numbers)

# 9.4 Challenge: List of lists
# Write a program that contains the following lists of lists:
# universities = [
# ['California Institute of Technology', 2175, 37704],
# ['Harvard', 19627, 39849],
# ['Massachusetts Institute of Technology', 10566, 40732],
# ['Princeton', 7802, 37000],
# ['Rice', 5879, 35551],
# ['Stanford', 19535, 40569],
# ['Yale', 11701, 40500]
# ]
# Define a function, enrollment_stats(), that takes, as an input, a list of lists where each individual list contains
# three elements: (a) the name of a university, (b) the total number of enrolled students, and (c) the annual tuition
# fees. enrollment_stats() should return two lists: the first containing all of the student enrollment values and the
# second containing all of the tuition fees. Next, define a mean() and a median() function. Both functions should
# take a single list as an argument and return the mean and median of the values in each list.
# Using universities, enrollment_stats(), mean(), and median(), calculate the total number of students, the total
# tuition, the mean and median of the number of students, and the mean and median tuition values.
# Finally, output all values, and format the output so that it looks like this:
# ******************************
# Total students: 77,285
# Total tuition: $ 271,905
# Student mean: 11,040.71
# Student median: 10,566
# Tuition mean: $ 38,843.57
# Tuition median: $ 39,849
# ******************************


def enrollment_stats(list_of_universities):
    total_students = []
    total_tuition = []

    for university in list_of_universities:
        total_students.append(university[1])
        total_tuition.append(university[2])

    return total_students, total_tuition


def mean(values):
    return sum(values) / len(values)


def median(values):
    values.sort()
    if len(values) % 2 == 1:
        center_index = int(len(values) / 2)
        return values[center_index]
    else:
        left_center_index = (len(values) - 1) // 2
        right_center_index = (len(values) + 1) // 2
        return mean([values[left_center_index], values[right_center_index]])


universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
    ]

totals = enrollment_stats(universities)

print("\n")
print("*****" * 6)
print(f"Total students:   {sum(totals[0]):,}")
print(f"Total tuition:  $ {sum(totals[1]):,}")
print(f"\nStudent mean:     {mean(totals[0]):,.2f}")
print(f"Student median:   {median(totals[0]):,}")
print(f"\nTuition mean:   $ {mean(totals[1]):,.2f}")
print(f"Tuition median: $ {median(totals[1]):,}")
print("*****" * 6)
print("\n")

# 9.5 Challenge: Wax Poetic
# In this challenge, you’ll write a program that generates poetry. Create five lists for different word types:
# • Nouns: ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
# • Verbs: ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
# • Adjectives: ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
# • Prepositions: ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
# • Adverbs: ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]
# Randomly select the following number of elements from each list:
# • 3 nouns
# • 3 verbs
# • 3 adjectives
# • 2 prepositions
# • 1 adverb
# You can do this with the choice() function in the random module. This function takes a list as input and returns a
# randomly selected element of the list.
# For example, here’s how you use random.choice() to get random element from the list ["a", "b", "c"]:
# import random
# random_element = random.choice(["a", "b", "c"])
# Using the randomly selected words, generate and display a poem with the following structure inspired by Clifford
# Pickover:
# {A/An} {adj1} {noun1}
# {A/An} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
# {adverb1}, the {noun1} {verb2}
# the {noun2} {verb3} {prep2} a {adj3} {noun3}
# Here, adj stands for adjective and prep for preposition. Here’s an example of the kind of poem your program might
# generate:
# A furry horse
# A furry horse curdles within the fragrant mango
# extravagantly, the horse slurps
# the mango meows beneath a balding extrovert
# Every time your program runs, it should generate a new poem.

import random

noun = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verb = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjective = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
preposition = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverb = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]


def make_poem():
    n1 = random.choice(noun)
    n2 = random.choice(noun)
    n3 = random.choice(noun)
    while n1 == n2:
        n2 = random.choice(noun)
    while n1 == n3 or n2 == n3:
        n3 = random.choice(noun)

    v1 = random.choice(verb)
    v2 = random.choice(verb)
    v3 = random.choice(verb)
    while v1 == v2:
        v2 = random.choice(verb)
    while v1 == v3 or v2 == v3:
        n3 = random.choice(verb)

    adj1 = random.choice(adjective)
    adj2 = random.choice(adjective)
    adj3 = random.choice(adjective)
    while adj1 == adj2:
        adj2 = random.choice(adjective)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjective)

    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)
    while prep1 == prep2:
        prep2 = random.choice(preposition)

    adv1 = random.choice(adverb)

    if "aeiou".find(adj1[0]) != -1:  # First letter is a vowel
        article = "An"
    else:
        article = "A"

    poem = (
        f"{article} {adj1} {n1}\n\n"
        f"{article} {adj1} {n1} {v1} {prep1} the {adj2} {n2}\n"
        f"{adv1}, the {n1} {v2}\n"
        f"the {n2} {v3} {prep2} a {adj3} {n3}"
    )

    return poem


poem = make_poem()
print(poem)

# 9.6 Store Relationships in Dictionaries
# 1. Create an empty dictionary named captains.
captains = {}
# 2. Using the square bracket notation, enter the following data into the dictionary, one item at a time:
# 'Enterprise': 'Picard'
# 'Voyager': 'Janeway'
# 'Defiant': 'Sisko'
captains['Enterprise'] = 'Picard'
captains['Voyager'] = 'Janeway'
captains['Defiant'] = 'Sisko'
# 3. Write two if statements that check if "Enterprise" and "Discovery" exist as keys in the dictionary.
# Set their values to "unknown" if the key does not exist.
if "Enterprise" in captains:
    print(captains["Enterprise"])
else:
    captains["Enterprise"] = "unknown"
    print(captains["Enterprise"])
if "Discovery" in captains:
    print(captains["Discovery"])
else:
    captains["Discovery"] = "unknown"
    print(captains["Discovery"])
# 4. Write a for loop to display the ship and captain names contained in the dictionary. For example, the output
# should look something like this:
# The Enterprise is captained by Picard.
for key in captains:
    print(f" The {key} is captained by {captains[key]}")
# 5. Delete "Discovery" from the dictionary.
del(captains["Discovery"])
# 6. Bonus: Make the same dictionary by using dict() and passing in the initial values when you first create the
# dictionary.
captains = dict(
    [
    ("Enterprise", "Picard"),
    ("Voyager", "Janeway"),
    ("Defiant", "Sisko")
    ]
)

# 9.7 Challenge: Capital City Loop
# Review your state capitals along with dictionaries and while loops! First, finish filling out the following dictionary
# with the remaining states and their associated capitals in a file called capitals.py.
# capitals_dict = {
# 'Alabama': 'Montgomery',
# 'Alaska': 'Juneau',
# 'Arizona': 'Phoenix',
# 'Arkansas': 'Little Rock',
# 'California': 'Sacramento',
# 'Colorado': 'Denver',
# 'Connecticut': 'Hartford',
# 'Delaware': 'Dover',
# 'Florida': 'Tallahassee',
# 'Georgia': 'Atlanta',
# }
# Next, pick a random state name from the dictionary, and assign both the state, and it’s capital to two variables.
# You’ll need to import the random module at the top of your program.
# Then display the name of the state to the user and ask them to enter the capital. If the user answers, incorrectly,
# repeatedly ask them for the capital name until they either enter the correct answer or type the word “exit”.
# If the user answers correctly, display "Correct" and end the program.
# However, if the user exits without guessing correctly, display the correct answer and the word "Goodbye".
# Note
# Make sure the user is not punished for case sensitivity. In other
# words, a guess of "Denver" is the same as "denver". Do the same
# for exiting—"EXIT" and "Exit" should work the same as "exit".

import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
 }

state, capital = random.choice(list(capitals_dict.items()))

while True:
    guess = input(f"What is the capital of '{state}'? ").lower()
    if guess == "exit":
        print(f"The capital of '{state}' is '{capital}'.")
        print("Goodbye")
        break
    elif guess == capital.lower():
        print("Correct! Nice job.")
        break

# 9.9 Challenge: Cats With Hats
# You have 100 cats.
# One day you decide to arrange all your cats in a giant circle. Initially, none of your cats have any hats on.
# You walk around the circle 100 times, always starting at the same spot, with the first cat (cat # 1). Every time you
# stop at a cat, you either put a hat on it if it doesn’t have one on, or you take its hat off if it has one on.
# 1. The first round, you stop at every cat, placing a hat on each one.
# 2. The second round, you only stop at every second cat (#2, #4, #6, #8, etc.).
# 3. The third round, you only stop at every third cat (#3, #6, #9, #12, etc.).
# 4. You continue this process until you’ve made 100 rounds around the cats (e.g., you only visit the 100th cat).
# Write a program that simply outputs which cats have hats at the end.
# Note
# This is not an easy problem by any means. Honestly, the code is simple. This problem is often seen on job interviews
# as it tests your ability to reason your way through a difficult problem. Stay calm. Start with a diagram, and then
# write pseudo code. Find a pattern. Then code!
cats_with_hats = [False] * 101

for round_number in range(1, 101):
    for cat_number in range(1, 101):
        if cat_number % round_number == 0:
            cats_with_hats[cat_number] = not cats_with_hats[cat_number]

for cat_number in range(1, 101):
    if cats_with_hats[cat_number]:
        print("Cat #", cat_number, "has a hat.")
